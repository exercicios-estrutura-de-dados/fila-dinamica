# Fila Dinâmica

Este é um exercício proposto na disciplina de **Estrutura de Dados 1** do [**Instituto de Informática**](https://inf.ufg.br/) da [**Universidade Federal de Goiás**](https://www.ufg.br/).

Consiste em na implementação de uma fila dinâmica com as seguintes funções:

- `criar_fila()` - criar uma nova fila vazia
- `push()` - insere um novo elemento no final da fila.
- `pop()` - remove o primeiro elemento da fila.
- `consultar_posicao()` - retorna a posição de um elemento na fila.
- `imprimir_fila()` - imprime todos os elementos da fila.

A fila dinâmica armazena as informações de processos fictícios. Exemplo de processo:
```c
PROCESSO processo = {
    .id = 104,
    .nome = "Window manager",
    .prioridade = 4,
    .tempoEspera = 200
}
```

O programa permite:
- Incluir novos processos na fila de processo
- Matar o processo com o maior tempo de espera
- Executar um processo (remover da fila)
- Imprimir todos os processos da fila

## Como compilar e executar?

Os requisitos são:
* [Git](https://git-scm.com/)
* [GNU Make](https://www.gnu.org/software/make/)
* Um Compilador para **C**
    * [GCC](https://gcc.gnu.org/) ou
    * [Clang](https://clang.llvm.org/) ou
    * [MinGW](https://www.mingw-w64.org/)

### Instalação das ferramentas
* No **Windows** utilizando [Chocolatey](https://chocolatey.org/install#individual).

Execute o comando no PowerShell
```powershell
> choco install git mingw make
```

* No **Ubuntu, Debian, Mint, PopOS**...</br>

Execute o comando no Terminal
```bash
$ sudo apt update && sudo apt install git build-essential
```

* No **MacOS** </br>

Instale [Command Line Tools](https://developer.apple.com/download/all/) ou [Xcode](https://developer.apple.com/download/all/)

### Compilando e executando
Basta clonar o repositório e utilizar o comando make para compilar
```bash
$ git clone https://gitlab.com/exercicios-estrutura-de-dados/fila-dinamica.git
$ cd fila-dinamica/
$ make
$ ./main
```

## License
This project is licensed under the terms of the [MIT](https://choosealicense.com/licenses/mit/) license.
