# Variávies
CC = gcc
CFLAGS = -pedantic-errors -Wall

# Regra : dependências
all: filaDinamica.o main.o
	$(CC) $(CFLAGS) main.o include/filaDinamica.o -o main

main.o: main.c
	$(CC) $(CFLAGS) -c main.c -o main.o

filaDinamica.o: include/filaDinamica.c
	$(CC) $(CFLAGS) -c include/filaDinamica.c -o include/filaDinamica.o

clean:
	rm *.o include/*.o main

run:
	./main

debug:
	make CFLAGS+=-g
