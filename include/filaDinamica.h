#ifndef _fila_dinamica_h_
#define _fila_dinamica_h_

/*************** tipos de dados ***********/

struct processo{
    int id;
    char nome[30];
    int prioridade;
    int tempoEspera;
}typedef PROCESSO;

typedef struct no_descritor FILA; // "Fila" é um nó descritor com as informações da fila

/*************** Funções da fila **************/

// Cria uma fila vazia
FILA* criar_fila();

// Verifica se a fila está vazia
int fila_esta_vazia(FILA* fila);

// Retorna tamanho da fila
int tamanho_fila(FILA* fila);

// Insere novo nó no final da fila
int push(FILA* fila, PROCESSO processo);

// Remove nó do início da fila
int pop(FILA* fila);

// Remover processo com maior tempo de espera
int matar_processo_maior_tempo(FILA* fila);

// Busca um processo pelo id e retorna sua posição na fila
int consultar_posicao(FILA* fila, int id);

// Imprime todos os elementos da lista
void imprimir_fila(FILA* fila);

#endif
