#include "filaDinamica.h"
#include <stdlib.h>
#include <stdio.h>

/*************** tipos de dados locais ***********/

struct elemento_da_fila{ // Define como é um elemento(nó) desta fila
    struct processo dados; // Guarda um processo
    struct elemento_da_fila *proximo; // Ponteiro para o próximo elemento da fila
}typedef ELEM;

struct no_descritor{ // Nó descritor com as informações da fila
    ELEM* inicio;
    ELEM* final;
    unsigned int qtd;
}typedef DESCRITOR;

/*************** Funções da fila **************/

// Cria uma fila vazia
FILA* criar_fila(){
    FILA* fila = (FILA*) malloc(sizeof(FILA));
    if(fila != NULL){ // Verifica se a fila existe, não é nula, foi alocada na memória
        fila->inicio = NULL;
        fila->final = NULL;
        fila->qtd = 0;
    }
    return fila;
}

// Verifica se a fila está vazia
int fila_esta_vazia(FILA* fila){
    if(fila == NULL || fila->qtd == 0){
        // Uma fila que não existe não pode estar cheia, por isso é retornado 1
        return 1; // Verdadeiro, a fila está vazia
    }else{
        return 0; // Falso, a fila contém itens
    }
}

// Retorna tamanho da fila
int tamanho_fila(FILA* fila){
    if(fila_esta_vazia(fila)){
        return 0;
    }else{
        return fila->qtd;
    }
}

// Insere novo nó no final da fila
int push(FILA* fila, PROCESSO processo){
    if(fila == NULL) return 0; // Verifica se a fila é nula (não existe)

    ELEM* noProcesso = (ELEM*)malloc(sizeof(ELEM));
    if(noProcesso == NULL) return 0; // Se nó está nulo, ocorreu erro ao alocar memória
    noProcesso->dados = processo; // Verifica se a fila é nula (não existe)
    noProcesso->proximo = NULL;

    if(fila_esta_vazia(fila)){ // Se a fila está vazia
        fila->inicio = noProcesso; // noProcesso é colocado no início
    }else{ // Se não, noProcesso vai para o final
        fila->final->proximo = noProcesso; // ... noAnterior → noFinal → noProcesso
    }
    fila->final = noProcesso; // Guarda o novo final da fila
    fila->qtd++; // Incrementa quantidade de elementos na fila

    return 1; // Se chegou até aqui, foi inserido com sucesso
}

// Remove nó do início da fila
int pop(FILA* fila){
    if(fila_esta_vazia(fila)) return 0; // Erro, não é possível remover de fila vazia

    ELEM *noAuxiliar = fila->inicio;
    if(tamanho_fila(fila) == 1){ // Temos apenas 1 elemento na fila? fila ficará vazia
        fila->inicio = NULL;
        fila->final = NULL;
    }else{
        fila->inicio = fila->inicio->proximo; // Próximo elemento vira o primeiro da fila
    }
    free(noAuxiliar); // Libera elemento que antes era o primeiro da fila
    fila->qtd--; // Decrementa quantidade de processos

    return 1; // Se chegou até aqui, foi removido com sucesso
}

// Remover processo com maior tempo de espera
int matar_processo_maior_tempo(FILA* fila){
    if(fila_esta_vazia(fila)) return 0; // Erro, a fila está vazia

    ELEM *noAuxiliar = fila->inicio, *noAnterior;
    if(tamanho_fila(fila) == 1){ // Remove único elemento
        fila->inicio = NULL;
        fila->final = NULL;
    }else{
        // Percorre analisando tempo de espera e guardando maior tempo encontrado
        int tempoEspera=-1;
        while(noAuxiliar!=NULL){
            if(noAuxiliar->dados.tempoEspera > tempoEspera) tempoEspera = noAuxiliar->dados.tempoEspera;
            noAuxiliar = noAuxiliar->proximo; // Incrementa e/ou aponta para o próximo elemento
        }
        // Procura o nó que tem maior tempo de espera
        noAuxiliar = fila->inicio, noAnterior = fila->inicio;
        while( noAuxiliar->dados.tempoEspera != tempoEspera){
            noAnterior = noAuxiliar;
            noAuxiliar = noAuxiliar->proximo; // Incrementa e/ou aponta para o próximo elemento
        }
        // Verifica resultado da busca
        if(noAuxiliar == fila->inicio){ // Estamo no início da fila: noAuxiliar → noSeguinte ...
            fila->inicio = noAuxiliar->proximo; // Próximo elemento vira começo da fila
        }
        else if(noAuxiliar == fila->final){ // Estamos no final da fila: ... noAnterior → noAuxiliar → NULL
            fila->final = noAnterior; // final → noAnterior
        }
        // Em qualquer posição da lista: noAnterior → noAuxiliar → noSeguinte
        noAnterior->proximo = noAuxiliar->proximo; // noAnterior → noSeguinte
    }
    free(noAuxiliar);
    fila->qtd--; // Decrementa quantidade de processos

    return 1;
}

// Busca um processo pelo id e retorna sua posição na fila
int consultar_posicao(FILA* fila, int id){
    if(fila_esta_vazia(fila) || id <= 0) return 0; // Erro, a fila está vazia ou id inválido

    ELEM* noAuxiliar = fila->inicio;
    int posicao;
    // Percorre fila procurando o processo
    for(posicao = 1; (noAuxiliar->proximo!=NULL && noAuxiliar->dados.id != id); posicao++){
        noAuxiliar = noAuxiliar->proximo; // Incrementa e/ou aponta para o próximo elemento
    }
    // Verifica resultado da busca
    if(noAuxiliar->dados.id == id){ // Encontrou?
        return posicao; // retorna posição
    }else{ // Não encontrou
        return -1;
    }
}

// Imprime todos os elementos da lista
void imprimir_fila(FILA* fila){
    if(fila_esta_vazia(fila)){
        printf("A fila está vazia. \n");
        return;
    }

    ELEM* noAuxiliar = fila->inicio;
     printf(
        "\n\033[7m"
        "+--------+--------------------------------+------------+----------------------+\n"
        "|   Id   |              Nome              | Prioridade | Tempo de Espera (ms) |\n"
        "+--------+--------------------------------+------------+----------------------+\033[0m\n"
    );
    while(noAuxiliar != NULL){
        printf(
            "|%7d | %-30s | %10d | %20d |\n"
            "+--------+--------------------------------+------------+----------------------+\n",
            noAuxiliar->dados.id,
            noAuxiliar->dados.nome,
            noAuxiliar->dados.prioridade,
            noAuxiliar->dados.tempoEspera
        );
        noAuxiliar = noAuxiliar->proximo;
    }
}
