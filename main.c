#include "include/filaDinamica.h"
#include <stdio.h>
#include <stdio.h>
#include <locale.h>
int primeiraExecucao=1, codigoRetornado, codePageAnterior;
FILA* filaProcessos;

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
    #include <windows.h>
#endif

void limparTela(){
    printf("\033[H\033[2J\033[3J");
}
void clearStdinBuffer(){ // Limpa o buffer de stdIn
    while(getchar() != '\n');
}
int main(){
    PROCESSO processo = {.id = 13, .nome = "desconhecido\0", .prioridade = 0, .tempoEspera = 0};
    unsigned int opcaoEscolhida;

    if(primeiraExecucao){
        #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
            codePageAnterior = GetConsoleCP(); // Guarda CodePage anterior
            SetConsoleCP(65001);  // Ativa UTF-8 como CodePage
            setlocale(LC_ALL, "pt_BR.UTF-8");
        #else
            setlocale(LC_ALL,"Portuguese");
        #endif
        filaProcessos = criar_fila();
        primeiraExecucao = 0;
    }

    printf(
        "\033[1;32m"
        "======== Menu principal ==========\033[0m\n"
        "1 - Inserir novo processo \n"
        "2 - Matar processo c/ maior tempo de espera \n"
        "3 - Executar um processo (remover da fila) \n"
        "4 - Imprimir todos os processo \n"
        "0 - Sair \n"
        ">: "
    );
    opcaoEscolhida=13;
    scanf("%1u", &opcaoEscolhida);
    clearStdinBuffer();

    switch(opcaoEscolhida){
        case 1:
            limparTela();
            printf(
                "\033[1;32m"
                "===== Inserir novo processo =====\033[0m\n"
            );
            printf("Id: ");
            scanf("%d", &processo.id);
            printf("Nome: ");
            scanf(" %[^\n]", processo.nome);
            printf("Prioridade: ");
            scanf("%d", &processo.prioridade);
            printf("Tempo de espera (ms): ");
            scanf("%d", &processo.tempoEspera);

            codigoRetornado = push(filaProcessos, processo);

            limparTela();
            if(codigoRetornado == 1){
                printf("Processo inserido com sucesso. \n");
            }else{
                printf("Não foi possível inserir o processo. \n");
            }
            main(); // Volta para main e mostra menu
            break;
        case 2:
            limparTela();
            printf(
                "\033[1;32m"
                "===== Matar processo c/ maior tempo de espera =====\033[0m\n"
            );
            codigoRetornado = matar_processo_maior_tempo(filaProcessos);
            
            limparTela();
            if(codigoRetornado == 1){
                printf("Sucesso ao matar processo. \n");
            }else{
                printf("Erro ao matar processo. \n");
            }
            main(); // Volta para main e mostra menu
            break;
        case 3:
            limparTela();
            printf(
                "\033[1;32m"
                "===== Executar um processo (remover da fila) =====\033[0m\n"
            );
            codigoRetornado = pop(filaProcessos);
            
            limparTela();
            if(codigoRetornado == 1){
                printf("Sucesso ao executar processo. \n");
            }else{
                printf("Erro ao executar processo. \n");
            }
            main(); // Volta para main e mostra menu
            break;
        case 4:
            limparTela();
            printf(
                "\033[1;32m"
                "===== Imprimir todos os processo =====\033[0m\n"
            );
            imprimir_fila(filaProcessos);

            printf("Pressione enter para continuar... \n");
            while(getchar() != '\n'); // Esperando "Enter" para continuar
            limparTela();
            main(); // Volta para main e mostra menu
            break;
        case 0:
            limparTela();
            printf("Saindo do programa... \n");
            #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
                SetConsoleCP(codePageAnterior); // Retorna o codePage ao estado anterior
            #endif
            break; // Sai do switch e finaliza o programa
        default:
            limparTela();
            printf("Opção inválida! Escolha novamente. \n");
            main(); // Volta para main e mostra menu
    };
    return 0;
}
